rdforms_specs.init({
    language: document.targetLanguage,
    bundles: [
      ['model2.0.json'],
    ],
    main: [
        'trails1.2-21',
        'trails1.2-16',
        'Sportsfacilities-91'
    ],
    supportive: [
      'Sportsfacilities-21'
    ]
  });
